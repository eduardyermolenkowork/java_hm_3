import java.util.*;

public class Main {
    enum DaysOfWeek {
        MONDAY("monday"),
        TUESDAY("tuesday"),
        WEDNESDAY("wednesday"),
        THURSDAY("thursday"),
        FRIDAY("friday"),
        SATURDAY("saturday"),
        SUNDAY("sunday");
        private String day;

        DaysOfWeek(String code) {
            this.day = code;
        }

        public String getVal() {
            return day;
        }
    }

    private static final int COUNT_DAYS_OF_WEEK = 7;

    public static void main(String[] args) throws Exception {
        String[][] scedule = new String[COUNT_DAYS_OF_WEEK][2];
        String[] tasksOfDays = {
                "Learning JAVA",
                "Computer science; walk; date with girl",
                "Do homework's; lesson in DAN-IT",
                "Write science work; walk",
                "lesson in DAN-IT",
                "Date with girl; drink, drink and drink more",
                "Seeping; rest; playing in computer"
        };
        for (int i = 0; i < DaysOfWeek.values().length; i++) {
            scedule[i][0] = DaysOfWeek.values()[i].getVal();
            scedule[i][1] = tasksOfDays[i];
        }

        while (true) {
            System.out.println("Please, input the day of the week:");
            String action = getActionFromCommand(getCommandFromInput(), scedule);
            if (isBreak(action)) break;
            System.out.println(action);
        }

    }

    public static void main2(String[] args) {
        System.out.println(getCommandFromInput());
    }

    public static boolean isBreak(String command) {
        return command.equals("exit");
    }


    public static String getCommandFromInput() {
        return new Scanner(System.in).nextLine().toLowerCase().trim();
    }

    public static String changeTask(String dayAndCommand, String[][] scedule) throws Exception {
        String[] actionOfChange = dayAndCommand.toLowerCase().trim().split("\\s++");
        String command = actionOfChange[0];
        String day = actionOfChange[1];
        int indexOfDay = getIndexOfDay(day);
        System.out.printf("Please, input new tasks for %s\n", day);
        scedule[indexOfDay][1] = getCommandFromInput();
        return "task updated successful";
    }

    public static int getIndexOfDay(String day) throws Exception {
        int indexOfDay = -1;
        for (int i = 0; i < COUNT_DAYS_OF_WEEK; i++) {
            if(day.toLowerCase().trim().equals(DaysOfWeek.values()[i].getVal())) {
                indexOfDay = i;
            }
        }
        if (indexOfDay == - 1) throw new Exception("Error: invalid name day of week");
        return indexOfDay;
    }

    public static boolean isChangeCommand(String inputCommand){
        //inputCommand = inputCommand.toLowerCase().trim();
        String[] actionOfChange = inputCommand.split("\\s++");
        System.out.println(Arrays.toString(actionOfChange));
        if (actionOfChange.length != 2) return false;
        String command = actionOfChange[0];
        String day = actionOfChange[1];
        if(command.equals("change")) {
            for (int i = 0; i < COUNT_DAYS_OF_WEEK; i++) {
                if(day.equals(DaysOfWeek.values()[i].getVal())) {
                    return true;
                }
            }
            return false;
        } else return false;
    };
    public static String getActionFromCommand(String command, String[][] scedule) throws Exception {
        if (command.equals("exit")) return "exit";
        for (int i = 0; i < COUNT_DAYS_OF_WEEK; i++) {
            if(command.equals(scedule[i][0])) return scedule[i][1];
        }
        if(isChangeCommand(command)) return changeTask(command, scedule);
        return "Sorry, I don't understand you, please try again.";
    }
}
